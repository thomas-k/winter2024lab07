public class SimpleWar {
	public static void main(String[] args){
		Deck stack = new Deck();
		stack.shuffle();
		
		int player1 = 0;
		int player2 = 0;
		
		while(stack.getNumCards() > 0){
			System.out.println("Next Round:");
			
			Card c1 = stack.drawTopCard();
			Card c2 = stack.drawTopCard();
			System.out.println("First player's card:\n" + c1 + "\nScore: " + c1.calculateScore());
			System.out.println("Secpnd player's card:\n" + c2 + "\nScore: " + c2.calculateScore());
			
			if(c1.calculateScore() > c2.calculateScore())
				player1++;
			else 
				player2++;
			
			System.out.println("Player 1 score: " + player1 + "\nPlayer 2 score: " + player2);
			System.out.println("=====================================");
		}
		
		if(player1 > player2) 
			System.out.println("Player 1 won! Congrats!");
		else if(player2 > player1) 
			System.out.println("Player 2 won! Congrats!");
		else
			System.out.println("It was a draw!");
	}
}