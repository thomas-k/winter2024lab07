public class Card{
	String suit;
	int value;
	
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	//score calculator
	
	public double calculateScore(){
		double score = this.value;
		if(this.suit.equals("hearts")) 
			score += 0.4;
		if(this.suit.equals("spades")) 
			score += 0.3;
		if(this.suit.equals("diamonds")) 
			score += 0.2;
		if(this.suit.equals("clubs")) 
			score += 0.1;
		
		return score;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	
	//toString method
	public String toString(){
		String output = "\u001B[47m";
		
		String card = "";
		if(this.suit.equals("hearts")){
			output += "\u001B[31m";
			card = "♥";
		}
		if(this.suit.equals("diamonds")){
			output += "\u001B[31m";
			card = "◆";
		}
		if(this.suit.equals("spades")){
			output += "\u001B[30m";
			card = "♠";
		}
		if(this.suit.equals("clubs")){
			output += "\u001B[30m";
			card = "♣";
		}
		
		String rank = Integer.toString(this.value);
		if(this.value == 1) rank = "A";
		if(this.value == 11) rank = "J";
		if(this.value == 12) rank = "Q";
		if(this.value == 13) rank = "K";
		output += card + rank + "\u001B[40m\u001B[37m";
		
		return output;
	}
	
}