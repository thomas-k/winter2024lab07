import java.util.*;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.cards = new Card[52];
		this.numberOfCards = cards.length;
		
		// suits array
		String[] suits = {"diamonds", "hearts", "spades", "clubs"};
		int index = 0;
		
		for(String s: suits){
			for(int i = 1; i <= 13; i++){
				this.cards[index] = new Card(s, i);
				index++;
			}
		}
	}
	
	public int length(){
		return numberOfCards;
	}
	
	public Card drawTopCard(){
		numberOfCards--;
		return cards[numberOfCards];
	}
	
	public void shuffle(){
		for(int i = 0; i < this.numberOfCards; i++){
			int rand = this.rng.nextInt(this.numberOfCards);
			Card placeholder = this.cards[i];
			this.cards[i] = cards[rand];
			cards[rand] = placeholder;
		}
	}
	
	public String toString(){
		String output = "";
		for(int i = 0; i < this.numberOfCards; i++){
			output += this.cards[i].toString() + "\n";
		}
		return output;
	}
	
	public int getNumCards(){
		return this.numberOfCards;
	}
}